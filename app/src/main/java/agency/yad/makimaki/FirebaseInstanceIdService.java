package agency.yad.makimaki;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;


public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {
  public static final String TAG = "TestFbseInstIdSvc";

  @Override
  public void onTokenRefresh() {
    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
    Log.d(TAG, "Refreshed token: " + refreshedToken);

    //~sendRegistrationToServer(refreshedToken);
  }

}