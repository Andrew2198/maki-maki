package agency.yad.makimaki;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;





import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void loadWebView(final String urlToOpen) {
        if (isOnline()) {
            webView = findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(urlToOpen,mapList);
            webView.setWebViewClient(new MyWebViewClient());
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Ошибка")
                    .setMessage("Похоже у вас нет соединения с интернетом")
                    .setCancelable(false)
                    .setNegativeButton("Попробовать еще раз",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    loadWebView(urlToOpen);
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private WebView webView;
    private Map<String,String> mapList=new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fabric.with(this, new Crashlytics());


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);



        Intent intent = getIntent();
        String openURL = intent.getStringExtra("openURL");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (openURL == null) {

            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
               @Override
                public void idsAvailable(String userId, String registrationId) {
                    Log.d("debug", "User:" + userId);

                   if (registrationId != null)
                       Log.d("debug", "registrationId:" + registrationId);
                       // SendIdUser(userId);

                   mapList.put("devise","android");
                   mapList.put("playerid",userId);

                    /*
                   mapList.put("userid",userId+" ,"+"player_id:"+userId);
                   mapList.put("blabla","blabla"+"player_id:"+userId);
                   mapList.put("blablanew"," "+userId);
                   mapList.put("nnnn"," "+userId);



                   URL url = null;
                    try {
                      //  url = new URL("http://makimaki.ru/device/?player_id=" + registrationId);
                        url = new URL("http://makimaki.ru/android/?player_id=" + registrationId);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    HttpURLConnection urlConnection = null;
                    try {
                        assert url != null;
                        urlConnection = (HttpURLConnection)  url.openConnection();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    assert urlConnection != null;
                    urlConnection.setRequestProperty("Accept", "application/json");
                    try {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        readStream(in);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        urlConnection.disconnect();
                    }
                    */

                }

                private String readStream(InputStream is) throws IOException {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
                    for (String line = r.readLine(); line != null; line =r.readLine()){
                        sb.append(line);
                    }
                    is.close();
                    return sb.toString();
               }
            });

            loadWebView("https://makimaki.ru");
           // loadWebView(" http://api.nbf.z1q.ru/header/add");
        } else {
            loadWebView(openURL);
        }
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private class MyWebViewClient extends WebViewClient {

        private CharSequence message;

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view.loadUrl(request.getUrl().toString());
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (request.getUrl().toString().startsWith("mailto:")) {

                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(share, "Написать email"));
                    view.reload();
                    return true;
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (request.getUrl().toString().startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(request.getUrl().toString()));
                    startActivity(intent);
                    view.reload();
                    return true;
                }
            }

            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            if (url.startsWith("mailto:")) {

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(share, "Написать email"));
                view.reload();
                return true;
            }

            if (url.startsWith("tel:")) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                startActivity(intent);
                view.reload();
                return true;
            }

            return true;
        }
    }

    private void SendIdUser (String userId){
        try {
            AndroidNetworking.get("http://makimaki.ru/android/?player_id={registrationId}")
                    .addPathParameter("registrationId", userId)
                   .addHeaders("registrationId",userId)
                   // .addBodyParameter("registrationId",userId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                        String resp=response.toString();
                        String resp1=response.toString();
                        }

                        @Override
                        public void onError(ANError anError) {
                        Log.i("eror",String.valueOf(anError));
                        }
                    });


        } catch (Exception e) {
            Log.i("TAG", String.valueOf(e));
        }

    }
}
